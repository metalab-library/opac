# Welcome

This is the documentation for the {{ config.site_name }}.

[![pipeline status](https://gitlab.com/metalab-library/opac/badges/main/pipeline.svg)](https://gitlab.com/metalab-library/opac/-/commits/main)

The Metalab has a library. And this library now has an OPAC! And this is the generator for it.

## What's an OPAC?

An [Online Public Access Catalogue](https://en.wikipedia.org/wiki/Online_public_access_catalog) is an online accessible library catalog.
